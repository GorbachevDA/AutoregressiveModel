import sys
sys.path.insert(0, './app')
from AutoregressModel import AutoregressModel
import matplotlib.pyplot as plt

N = 5000
D = 1000 # Кол - во коэффициентов
M = 2001 # must be > D * 2 Кол - во рассматриваемых точек
INPUT_FILE = 'input.txt'

data_c = [] # Полученные константы авторегрессионной модели
data_prediction = [] # Предсказанные данные
square_error = [] # Среднеквадратичная ошибка

model = AutoregressModel.initFromFile(open(INPUT_FILE), N, D, M)

square_error = []
for i in range(1, D + 1):
    model.setD(i)
    data_prediction, error = model.generatePrediction(N)
    square_error.append(error)
  
fig0 = plt.figure()
plt.grid()
plt.plot(square_error, '-*')
fig1 = plt.figure()
plt.grid()
plt.plot(data_prediction)
plt.show()

