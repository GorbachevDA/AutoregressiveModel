import matplotlib.pyplot as plt
import numpy as np
np.seterr(all='warn')

N = 1000
D = 127 # 32, 127, 287
INPUT_FILE = 'input.txt'

TEST_N = 10 # Кол-во сгенерированных точек
TEST_NOISE = 0.000 # Дисперсия шума
TEST_INITIALS = [2, 6] # Начальные точки
TEST_PARAMS = [0.15, 0.8] # Константы модели
TEST_PARAMS.reverse() # Для наглядности

test_c = [] # Тестовые константы в авторегрессионной модели

data = [] # Данные для анализа
data_c = [] # Полученные константы авторегрессионной модели
data_prediction = [] # Предсказанные данные
square_error = [] # Среднеквадратичная ошибка

def generateAutoRegressData(n, initials = [], params = [], noise=0):

    len_initials = len(initials);
    x = initials
    
    if ((len_initials>n) or (len_initials != len(params))):
        return None
    
    for i in range(len_initials - 1, n - 1):
        s = 0;
        for j in range(len_initials):
            try:
                 s += x[i - j]*params[j]
            except RuntimeWarning:
                 s += 0
        x.append(s + np.random.normal(0,noise))
    return x

def findRegressConstant(d, x = []):
    len_x = len(x)
    c = []
    if (len_x < d * 2):
        return None
    b = []
    a = []
    for i in range(d):
        b.append(x[d + i])
        a.append(x[i:d + i])
    c = np.linalg.lstsq(a, b)
    return c[0]

def getDataFromFile(path, n):
    res = []
    f = open(path)
    for i in range(n):
        line = f.readline()
        if len(line) == 0:
            break
        res.append(float(line))
    f.close()
    return res

def average(x,n):
    s = 0
    len_x = len(x)
    for i in range(n):
        s += x[i]
    return s/len_x

def dispersion(x,n):
    avr_x = average(x,n)
    len_x = len(x)
    s = 0
    for i in range(n):
        s += (x[i] - avr_x)**2
    return s/(len_x - 1)

def squareError(x,x_mod,d):
    len_x = len(x)
    s = 0
    for i in range(len_x):
        try:
            s += (x[i] - x_mod[i])**2
        except RuntimeWarning:
            s += 0
    disp = dispersion(x,d)
    return (s/(len_x - d))/disp

##test_signal = generateAutoRegressData(TEST_N,TEST_INITIALS,TEST_PARAMS,TEST_NOISE)
##test_c = findRegressConstant(2,test_signal)
##print(test_signal,len(test_signal))
##print(test_c)

data = getDataFromFile(INPUT_FILE,N)
for i in range(1,D + 1):
    data_c = findRegressConstant(i,data)
    data_prediction = generateAutoRegressData(N,data[:i],data_c)
    square_error.append(squareError(data,data_prediction,i))

fig0 = plt.figure()
plt.grid()
plt.plot(square_error)
fig1 = plt.figure()
plt.grid()
plt.plot(data_prediction)
fig2 = plt.figure()
plt.grid()
plt.plot(data)
plt.show()
