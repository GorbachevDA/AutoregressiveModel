import random
import numpy as np
np.seterr(all='warn')

class AutoregressModel:
    '''
    Авторегрессионная модель, где предсказанный элемент X' равен сумме от i=0 до D * 2 произведений Ci*Xn-i
    константы расчитаваются методом Гауса при помощи модуля numpy.linalg.lstsq с выводом ошибки при расчете
    '''

    _data = [] # Данные для анализа
    _D = 0 # Кол-во констант
    _M = 0 # Кол-во начальных точек, взятых для расчета

    def __init__(self, data:list, D:int, M:int):
        '''
        :param list data:
        :param int D:
        :param int M:
        '''
        self._data = data
        self._D = D
        self._M = M

    @staticmethod
    def init(data:list, D:int, M:int):
        '''
        Метод для инициализации по списку data по коэффициентма D и рассматриваемым занчениям M
        :param list data:
        :param int D:
        :param int M:
        :return AutoregressModel:
        '''
        if (D*2 >= M):
            raise ValueError('Параметр M должнен быть в 2 раза больше чем параметр D')
        if (len(data) < M):
            raise ValueError('Параметр M должен быть меньше чем кол-во данных')
        return AutoregressModel(data, D, M)

    @staticmethod
    def initFromFile(f:any, n:int, D:int, M:int):
        '''
        Считывает n значений из файла f
        :param file f:
        :param int n:
        :param int D:
        :param int M:
        :return list:
        '''
        res = []
        for i in range(n):
            line = f.readline()
            if len(line) == 0:
                break
            res.append(float(line))
        f.close()
        return AutoregressModel.init(res, D, M)

    @staticmethod
    def generateAutoRegressData(n:int, initials:list, constants:list, noise:float=0):
        '''
        # Создает данные на основе авторегрессионной модели n эл-ов по начальному списку initils и списку params
         с возможным добавлением шума
        :param int n:
        :param list initials:
        :param list constants:
        :param float noise:
        :return list:
        '''
        len_initials = len(initials)
        x = initials
        if ((len_initials > n) or (len_initials != len(constants))):
            raise ValueError('Начальных данных должно быть не меньше чем констант для построения')
        for i in range(len_initials - 1, n - 1):
            x.append(AutoregressModel.predictNext(initials[i - 1:len_initials + i - 1], constants) + random.normalvariate(0, noise))
        return x

    @staticmethod
    def predictNext(initials: list, constants: list):
        '''
        Возвращает расчет n+1 элемента
        :param list initials:
        :param lsit constants:
        :return:
        '''
        s = 0
        len_initials = len(initials)
        for i in range(len_initials):
            s += initials[(len_initials - 1) - i] * constants[i]
        return s

    def setData(self, data:list):
        self._data = data

    def setM(self, M:int):
        self._M = M

    def setD(self, D:int):
        self._D = D

    def findRegressConstants(self):
        '''
        Находит M - D констант авторегрессионной модели, возвращает кортеж из списка констант и среднеквадратичной ошибки
        :param list data
        :return tuple:
        '''
        data = self._data[0:self._M]
        len_data = len(data)
        b = []
        a = []
        for i in range(len_data - self._D):
            b.append(data[self._D + i])
            a.append(data[i:self._D + i])
        c = np.linalg.lstsq(a, b)
        return c[0][::-1], c[1] / (len_data - self._D) / np.var(data)

    def generatePrediction(self, n):
        '''
        Строит предсказание на n элементов
        :param int n:
        :return:
        '''
        m = self._D * 2 # Кол - во констант для построения модели
        prediction = self._data[:m]
        initial_len = len(prediction)
        if (n < initial_len):
            raise ValueError('Кол-во точек для построения предсказания n должно быть больше чем D*2')
        data_c, err = self.findRegressConstants()
        for i in range(0, n - initial_len):
            prediction.append(AutoregressModel.predictNext(self._data[i + self._D:m + i], data_c))
        return prediction, err

##def average(x,n): #TODO delete me
##    s = 0
##    len_x = len(x)
##    for i in range(n):
##        s += x[i]
##    return s/len_x
##
##def dispersion(x,n): #TODO delete me
##    avr_x = average(x,n)
##    len_x = len(x)
##    s = 0
##    for i in range(n):
##        s += (x[i] - avr_x)**2
##    return s/(len_x - 1)
##
##def squareError(x,x_mod,d): #TODO delete me
##    len_x = len(x)
##    s = 0
##    for i in range(len_x):
##        try:
##            s += (x[i] - x_mod[i])**2
##        except RuntimeWarning:
##            s += 0
##    disp = dispersion(x,d)
##    return (s/(len_x - d))/disp


##def generatePredictionsWithRefresh(signal,n,d):
##    prediction = signal[:d*2]
##    for i in range(0,n - d - 1):
##        data_c = findRegressConstant(d,signal[i:d*2 + i])
##        prediction.append(doPrediction(signal[i+d:d*2 + i],data_c))
##    return prediction