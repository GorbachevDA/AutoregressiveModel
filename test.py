import sys
sys.path.insert(0, './app')
from AutoregressModel import AutoregressModel

TEST_N = 50 # Кол-во сгенерированных точек
TEST_NOISE = 0.000 # Дисперсия шума
TEST_INITIALS = [2, 6] # Начальные точки
TEST_PARAMS = [0.8, -0.15] # Константы модели
TEST_D = 2
TEST_M = 10

test_data = AutoregressModel.generateAutoRegressData(TEST_N, TEST_INITIALS, TEST_PARAMS, TEST_NOISE)
model = AutoregressModel.init(test_data, TEST_D, TEST_M)
test_c = model.findRegressConstants()
test_pred = model.generatePrediction(TEST_N)

print(test_data, len(test_data))
print(test_c)
print(test_pred)